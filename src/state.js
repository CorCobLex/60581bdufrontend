import {createStore} from "vuex";
import router from "./router";

const backendUrl = process.env.VUE_APP_BACKEND_URL;
const client_id = process.env.VUE_APP_CLIENT_ID;
const client_secret = process.env.VUE_APP_CLIENT_SECRET;

const store = createStore({
    state: {
        user: [],
        token: null,
        preLoading: false,
        dataPreLoading: true,
        loginError: false,
        loggedIn: false,
        inst: [],
        pager: {
            currentPage: 1,
            perPage: 5,
        },
        validation: {},
        createInstDialogVisible: false,
    },
    mutations: {
        setToken(state, token) {
            state.token = token;
        },

        getToken(context, token) {
            context.token = token;
        },

        setUser(context, user) {
            context.user = user;
        },

        setPreloading(context, is_load) {
            context.preLoading = is_load;
        },
        setDataPreloading(context, is_load) {
            context.dataPreLoading = is_load;
        },
        setLoginError(context, isError) {
            context.loginError = isError
        },
        setLoggedIn(context, isLoggedIn) {
            context.loggedIn = isLoggedIn
        },
        setInst(context, inst) {
            context.inst = inst
        },
        setPage(context, page) {
            context.pager.currentPage = page
        },
        setPager(context, pager) {
            context.pager = pager
        },
        setPerPage(context, rows) {
            context.pager.perPage = rows
        },
        setValidation(context, validation) {
            context.validation = validation;
        },
        setCreateInstDialogVisible(context, visible) {
            context.createInstDialogVisible = visible;
        },
        logout(context) {

            context.user = null;
            context.token = null;
            context.loggedIn = false;
            context.inst = {};
            localStorage.removeItem('token')
            router.push('/')
        },
    },
    actions: {
        auth(context, {login, password}) {

            context.commit('setPreloading', true)
            window.axios.post(backendUrl + '/OAuthController/Authorize', {
                username: login,
                password: password,
                grant_type: 'password'
            }, {
                headers: {
                    Authorization: 'Basic ' + window.btoa(client_id + ':' + client_secret)
                }
            }).then((response) => {
                if (response.data.access_token) {
                    context.commit('setToken', response.data.access_token)
                    localStorage.setItem('token', response.data.access_token);
                    context.commit('setLoggedIn', true);
                    context.commit('setLoginError', false);
                    context.dispatch('getUser');
                } else {
                    context.commit('setLoginError', true)
                    context.commit('setPreloading', false)
                }
            })

        },
        getUser(context) {
            // context.commit('setPreloading', true);
            console.log('get user')
            return window.axios.get(backendUrl + '/OAuthController/user', {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            }).then((response) => {
                context.commit('setUser', response.data);
                context.commit('setPreloading', false);

            }).catch(error => {
                    if (error.response) context.commit('setLoggedIn', false);
                }
            )

        },
        getInst(context) {
            console.log('get inst')
            context.commit('setDataPreloading', true)
            const params = new URLSearchParams()
            params.append('per_page', context.state.pager.perPage)
            return window.axios.post(backendUrl + '/InstApi/inst?page_group1=' + context.state.pager.currentPage, params,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                context.commit('setInst', response.data.insts);
                context.commit('setPager', response.data.pager);
                context.commit('setDataPreloading', false);
                console.log(response.data.insts)
                console.log(response.data.pager)

            })
        },
        createInst(context, {instmodel, fabricator, price, manufactory, wearRate, picture}) {
            console.log('create inst')
            context.commit('setDataPreloading', true)

            let formData = new FormData();
            formData.append('instmodel', instmodel)
            formData.append('fabricator', fabricator)
            formData.append('price', price)
            formData.append('Manufacture_country', manufactory)
            formData.append('Wear_rate', wearRate)
            formData.append('picture', picture);
            return window.axios.post(backendUrl + '/InstApi/store', formData,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                if (response.status === 201) {
                    console.log("inst created successfully");
                    context.commit("setValidation", {})
                    context.commit('setCreateInstDialogVisible', false);
                    context.commit('setPage', context.state.pager.pageCount);
                    this.dispatch('getInst');
                    router.push('/inst');
                } else {
                    console.log(response.data)
                    context.commit("setValidation", response.data)
                }
            })
        },

        deleteInst(context, {id}) {
            console.log('delete inst')
            context.commit('setDataPreloading', true)
            return window.axios.get(backendUrl + '/InstApi/delete/' + id,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                if (response.status === 200) {
                    console.log("Inst deleted successfully");
                    context.commit('setPage', context.state.pager.pageCount);
                    this.dispatch('getInst');
                    router.push('/inst');
                } else {
                    console.log(response.data)
                }
            })
        }
    }
})

export default store;