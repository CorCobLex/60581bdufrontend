import { createRouter, createWebHistory } from 'vue-router'
import Homepage from './components/Homepage'
import Inst from "./components/Inst";
import LoginDialog from "./components/LoginDialog";

const routes = [
    {
        path: '/',
        component: Homepage
    },

    {
        path: '/inst',
        component: Inst,
    },
    {
        path: '/login',
        component: LoginDialog,
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes
})
export default router;